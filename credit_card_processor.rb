class CreditCardProcessor
    attr_reader :accounts
  
    def initialize
      @accounts = {}
    end
  
    def process_input(input)
      command, *args = input.split
      case command
      when "Add"
        add_account(*args)
      when "Charge"
        charge(*args)
      when "Credit"
        credit(*args)
      else
        puts "Invalid command: #{command}"
      end
    end
  
    def add_account(name, card_number, limit)
      if validate_card_number(card_number)
        @accounts[name] = { card_number: card_number, balance: 0, limit: limit.delete('$').to_i }
      else
        puts "#{name}: error"
      end
    end
  
    def charge(name, amount)
      return unless @accounts[name]
  
      amount = amount.delete('$').to_i
      @accounts[name][:balance] += amount if (@accounts[name][:balance] + amount) <= @accounts[name][:limit]
    end
  
    def credit(name, amount)
      return unless @accounts[name]
  
      amount = amount.delete('$').to_i
      @accounts[name][:balance] -= amount
    end
  
    def generate_summary
      @accounts.keys.sort.each do |name|
        balance = @accounts[name][:balance]
        if validate_card_number(@accounts[name][:card_number])
          puts "#{name}: $#{balance}"
        else
          puts "#{name}: error"
        end
      end
    end
  
    private
  
      def validate_card_number(card_number)
        digits = card_number.gsub(/\s+/, "").chars.map(&:to_i)
        return false if digits.size < 2
  
        check_digit = digits.pop
        sum = digits.reverse.each_slice(2).flat_map do |x, y|
          [(x * 2).divmod(10), y || 0]
        end.flatten.inject(:+)
        (10 - sum % 10) == check_digit
    end
  end
  