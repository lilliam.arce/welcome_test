require_relative 'credit_card_processor'


if __FILE__ == $PROGRAM_NAME
    processor = CreditCardProcessor.new
  
    ARGF.each_line do |line|
      processor.process_input(line.chomp)
    end
  
    processor.generate_summary
end