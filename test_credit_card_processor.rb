require 'minitest/autorun'
require_relative 'credit_card_processor'

class CreditCardProcessorTest < Minitest::Test
  def setup
    @processor = CreditCardProcessor.new
  end

  def test_add_account
    @processor.add_account("Tom", "4111111111111111", "$1000")
    assert_equal({ "Tom" => { card_number: "4111111111111111", balance: 0, limit: 1000 } }, @processor.accounts)
  end

  def test_charge_valid
    @processor.add_account("Tom", "4111111111111111", "$1000")
    @processor.charge("Tom", "$500")
    assert_equal 500, @processor.accounts["Tom"][:balance]
  end

  def test_charge_exceed_limit
    @processor.add_account("Tom", "4111111111111111", "$1000")
    @processor.charge("Tom", "$1500")
    assert_equal 0, @processor.accounts["Tom"][:balance]
  end

  def test_charge_invalid_card_number
    @processor.add_account("Tom", "4111111111111112", "$1000")
    @processor.charge("Tom", "$500")
    assert_nil @processor.accounts["Tom"]
  end

  def test_credit_valid
    @processor.add_account("Tom", "4111111111111111", "$1000")
    @processor.charge("Tom", "$500")
    @processor.credit("Tom", "$300")
    assert_equal 200, @processor.accounts["Tom"][:balance]
  end

  def test_credit_negative_balance
    @processor.add_account("Tom", "4111111111111111", "$1000")
    @processor.charge("Tom", "$500")
    @processor.credit("Tom", "$700")
    assert_equal(-200, @processor.accounts["Tom"][:balance])
  end

  def test_credit_invalid_card_number
    @processor.add_account("Tom", "4111111111111112", "$1000")
    @processor.credit("Tom", "$500")
    assert_nil @processor.accounts["Tom"]
  end
end